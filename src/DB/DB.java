package DB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DB {

    private static Connection con;
    private static String host;

    public static Connection getconn() {
        if (con == null) {
            String hostname = "";
            String port = "";
            String db = "";
            String userName = "";
            String password = "";

            try {
                String filePath = ("C:/stic99/host.txt");
                // String filePath = ("/home/anthonydonx/Desktop/NewFolder/stocktack/host.txt");
                FileReader fr = new FileReader(filePath);
                BufferedReader bw = new BufferedReader(fr);
                //SET READ DATA TO THE CHAR ARRAY
                String line = bw.readLine();
                String[] all = line.split("-");
                hostname = all[0];
                port = all[1];
                db = all[2];
                userName = all[3];
                password = all[4].trim();

                System.out.println(password);

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            try {
                Class.forName("com.mysql.jdbc.Driver");
                ///  System.out.println("jdbc:mysql://" + host);
                con = DriverManager.getConnection("jdbc:mysql://" + hostname + ":" + port + "/" + db + "", userName, password);

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
        return con;
    }

    public static void change(String s) {
        if (con == null) {
            getconn();
        }
        try {
            con.createStatement().executeUpdate(s);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public static ResultSet search(String s) throws SQLException {
        if (con == null) {
            getconn();

        }

        return con.createStatement().executeQuery(s);

    }
//public static  int  getCount(String s) throws SQLException{
//        if (con==null) {
//            getconn();
//
//        }
//
//        return con.createStatement().executeQuery(s);
//
//}
}
